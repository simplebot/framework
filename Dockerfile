FROM microsoft/dotnet:2.1-sdk

ADD . /src/
RUN mkdir -p /app && dotnet build -c Release -o /app/ /src/Framework/

RUN mv /src/run.sh /app/run.sh && chmod +x /app/run.sh

WORKDIR /app/
CMD ["sh", "run.sh"]