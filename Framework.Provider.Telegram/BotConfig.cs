﻿using Telegram.Bot.Types.Enums;

namespace SimpleBot.Framework.Provider.Telegram
{
    public class BotConfig
    {
        public string ApiKey { get; set; }
        public ProxyConfig Proxy { get; set; }
        public int MessageMaxLength { get; set; } = 4096;
        public bool HandleEditedMessages { get; set; } = true;
        public ParseMode ParseMode { get; set; } = ParseMode.Markdown;
    }
}