using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MihaZupan;
using SimpleBot.Framework.Interfaces;
using SimpleBot.Framework.Models;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;

namespace SimpleBot.Framework.Provider.Telegram
{
    public class TelegramBotProvider : IBotProvider
    {
        private readonly Dictionary<long, int> _chatLastMessage;
        private readonly ILogger<TelegramBotProvider> _logger;
        private readonly TelegramBotClient _telegramClient;
        private readonly BotConfig _config;

        public event EventHandler<Context> OnReceiveMessage = (s, e) => { };
        public event EventHandler OnReady = (s, e) => { };

        public TelegramBotProvider(IOptions<BotConfig> cfg, ILogger<TelegramBotProvider> logger)
        {
            _chatLastMessage = new Dictionary<long, int>();
            _logger = logger;
            _config = cfg.Value;

            if (_config.Proxy != null)
            {
                _telegramClient = new TelegramBotClient(_config.ApiKey, new HttpToSocks5Proxy(
                    _config.Proxy.Host,
                    _config.Proxy.Port,
                    _config.Proxy.UserName,
                    _config.Proxy.Password)
                {
                    ResolveHostnamesLocally = true
                });
            }
            else
            {
                _telegramClient = new TelegramBotClient(_config.ApiKey);
            }
        }

        public async Task RunAsync()
        {
            _telegramClient.OnMessage += ReceiveMessage;
            _telegramClient.OnCallbackQuery += TelegramClientOnCallbackQuery;
            _telegramClient.OnInlineQuery += TelegramClientOnOnInlineQuery;
            _telegramClient.OnInlineResultChosen += TelegramClientOnOnInlineResultChosen;

            if (_config.HandleEditedMessages)
                _telegramClient.OnMessageEdited += ReceiveMessage;

            _logger.LogInformation("Begin connection...");
            var me = await _telegramClient.GetMeAsync();

            _logger.LogInformation($"Connected, my name is {me.FirstName}");
            _logger.LogInformation($"Start receiving");
            _telegramClient.StartReceiving();
            
            OnReady?.Invoke(this, null);
        }

        private void TelegramClientOnOnInlineResultChosen(object sender, ChosenInlineResultEventArgs e)
        {
            var user = e.ChosenInlineResult.From;

            var context = new Context
            {
                Text = e.ChosenInlineResult.Query,
                Username = user.Username,
                InlineQuery = InlineQueryMode.Result,
                ChatId = user.Id,
                ChatTitle = user.Username,
                Provider = this
            };

            OnReceiveMessage(this, context);
        }

        private void TelegramClientOnOnInlineQuery(object sender, InlineQueryEventArgs e)
        {
            var user = e.InlineQuery.From;

            var context = new Context
            {
                Text = e.InlineQuery.Query,
                Username = user.Username,
                InlineQuery = InlineQueryMode.Query,
                ChatId = user.Id,
                ChatTitle = user.Username,
                Provider = this
            };

            OnReceiveMessage(this, context);
        }

        private void TelegramClientOnCallbackQuery(object sender, CallbackQueryEventArgs e)
        {
            var msg = e.CallbackQuery.Message;

            var context = new Context
            {
                Text = e.CallbackQuery.Data,
                Username = msg.From.Username,
                IsGroupChat = msg.Chat.Type != ChatType.Private,
                ChatId = msg.Chat.Id,
                ChatTitle = msg.Chat.Title ?? msg.Chat.Username,
                Provider = this
            };

            OnReceiveMessage(this, context);
        }

        public void ReceiveMessage(object sender, MessageEventArgs e)
        {
            var msg = e.Message;

            var fileGetter = msg.Document != null
                ? new Func<Task<byte[]>>(() => DownloadFile(msg.Document.FileId))
                : null;

            var context = new Context
            {
                Text = msg.Text ?? msg.Caption,
                Username = msg.From.Username,
                IsGroupChat = msg.Chat.Type != ChatType.Private,
                ChatId = msg.Chat.Id,
                ChatTitle = msg.Chat.Title ?? msg.Chat.Username,
                Provider = this,
                FileName = e.Message.Document?.FileName,
                FileGetter = fileGetter
            };

            OnReceiveMessage(this, context);
        }

        private async Task<byte[]> DownloadFile(string fileId)
        {
            var buffer = new MemoryStream();
            await _telegramClient.GetInfoAndDownloadFileAsync(fileId, buffer);

            return buffer.ToArray();
        }

        public async Task SendMessageAsync(Context context, string message)
        {
            var offset = 0;
            for (; offset < message.Length - _config.MessageMaxLength; offset += _config.MessageMaxLength)
            {
                var path = message.Substring(offset, _config.MessageMaxLength);
                await _telegramClient.SendTextMessageAsync(context.ChatId, path, _config.ParseMode);
            }

            message = message.Substring(offset, message.Length - offset);
            var markup = GetKeyboardMarkup(context);
            
            var msg = await _telegramClient.SendTextMessageAsync(context.ChatId, message, _config.ParseMode, replyMarkup: markup);
            _chatLastMessage[context.ChatId] = msg.MessageId;
        }

        public async Task EditLastMessageAsync(Context context, string message)
        {
            if (!_chatLastMessage.TryGetValue(context.ChatId, out var messageId))
            {
                await SendMessageAsync(context, message);
                return;
            }

            var markup = GetKeyboardMarkup(context);
            await _telegramClient.EditMessageTextAsync(context.ChatId, messageId, message, replyMarkup: markup);
        }

        private static InlineKeyboardMarkup GetKeyboardMarkup(Context context)
        {
            InlineKeyboardMarkup markup = null;
            if (context.Buttons != null)
            {
                markup = new InlineKeyboardMarkup(context.Buttons.Select(x => x.Select(b => new InlineKeyboardButton
                {
                    Text = b.Text,
                    Url = b.Url,
                    CallbackData = b.Data
                })));
            }

            return markup;
        }

        public async Task SendFileAsync(Context context, string filename, Stream data)
        {
            await _telegramClient.SendDocumentAsync(context.ChatId, new InputOnlineFile(data, filename));
        }
    }
}