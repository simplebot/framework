using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace SimpleBot.Framework.Provider.Vk
{
    public class LongPollWatcher
    {
        private int _currentSleepSteps = 1;
        private readonly ILogger _logger;
        private readonly VkApi _api;
        private Timer _watchTimer;

        public bool Active { get; set; }
        public ulong? Ts { get; set; }
        public ulong? Pts { get; set; }
        public uint LongPollVersion { get; set; } = 3;
        public long? HistoryPreviewLength { get; set; }

        public int StepSleepTimeMsec { get; set; } = 10;
        public int MaxSleepSteps { get; set; } = 10;

        public event EventHandler<IEnumerable<Message>> OnMessages;

        public LongPollWatcher(ILogger logger, VkApi api)
        {
            _logger = logger;
            _api = api;
        }

        private async Task<LongPollServerResponse> GetLongPollServerAsync()
        {
            _logger.LogDebug($"Start retrieving Long Poll Server.");
            try
            {
                var server = await _api.Messages.GetLongPollServerAsync(!Pts.HasValue, LongPollVersion);

                if (ulong.TryParse(server.Ts, out var ts))
                    Ts = ts;
                
                if (server.Pts.HasValue)
                    Pts = server.Pts;

                return server;
            }
            catch (Exception ex)
            {
                _logger.LogError(
                    $"Error retrieving Long Poll Server{(_api.UserId.HasValue ? $" for user {_api.UserId.Value}" : "")}.{Environment.NewLine}" +
                    $"{(ex.InnerException != null ? ex.InnerException.Message : ex.Message)}");
                throw;
            }
        }

        private async Task<LongPollHistoryResponse> GetLongPollHistoryAsync()
        {
            if (!Ts.HasValue)
                await GetLongPollHistoryAsync();

            var req = new MessagesGetLongPollHistoryParams
            {
                Ts = Ts.Value,
                Pts = Pts,

                EventsLimit = 2000,
                Fields = UsersFields.Nickname,
                MaxMsgId = null,

                PreviewLength = HistoryPreviewLength,
                Onlines = false,
                LpVersion = LongPollVersion
            };
            while(true)
                try
                {
                    var server = await _api.Messages.GetLongPollHistoryAsync(req);
                    Pts = server.NewPts;
                    return server;
                }
                catch (Exception e)
                {
                    _logger.LogError(e.ToString());
                    await Task.Delay(1000);
                }
        }

        private async Task WatchStepAsync(object state)
        {
            try
            {
                var server = await GetLongPollHistoryAsync();
                if (server.Messages.Count > 0)
                {
                    _currentSleepSteps = 1;
                    OnMessages?.Invoke(this, server.Messages);
                }
                else
                {
                    if (_currentSleepSteps < MaxSleepSteps)
                        _currentSleepSteps++;
                }

                if(!Active)
                    return;
                
                _watchTimer.Change(_currentSleepSteps * StepSleepTimeMsec, Timeout.Infinite);
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
            }
        }

        public async Task StartWatchAsync()
        {
            if (Active)
            {
                _logger.LogDebug($"Attemption to start active watcher.");
                return;
            }

            Active = true;
            _logger.LogDebug($"Starting watcher{(Pts.HasValue ? $" with Pts: {Pts.Value}" : "")}.");

            await GetLongPollServerAsync();
            _watchTimer = new Timer(async x => await WatchStepAsync(x), null, 0, Timeout.Infinite);
        }

        public void StopWatch()
        {
            if (!Active)
                return;

            Active = false;
            _logger.LogDebug($"Watcher paused{(Ts.HasValue ? $" on TS:{Ts.Value}" : "")}.");
        }
    }
}