using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SimpleBot.Framework.Interfaces;
using SimpleBot.Framework.Models;
using VkNet;
using VkNet.Enums;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace SimpleBot.Framework.Provider.Vk
{
    public class VkBotProvider : IBotProvider
    {
        private readonly LongPollWatcher _longPollWatcher;
        private readonly ILogger<VkBotProvider> _logger;
        private readonly BotConfig _cfg;
        private readonly VkApi _api;
        private Timer _authTimer;

        public event EventHandler OnReady = (s, c) => { };
        public event EventHandler<Context> OnReceiveMessage = (s, e) => { };

        public VkBotProvider(IOptions<BotConfig> cfg, ILogger<VkBotProvider> logger)
        {
            _api = new VkApi();
            _longPollWatcher = new LongPollWatcher(logger, _api);
            _longPollWatcher.OnMessages += ReceiveMessages;

            _logger = logger;
            _cfg = cfg.Value;
        }

        public async Task RunAsync()
        {
            var periodMs = _cfg.ReloginPeriosSec * 1000;
            _authTimer = new Timer(async x => await Relogin(), null, periodMs, periodMs);
            await Login();
            
            OnReady?.Invoke(this, null);
        }

        private async Task Login()
        {
            _logger.LogInformation("Authorize...");
            await _api.AuthorizeAsync(new ApiAuthParams {AccessToken = _cfg.ApiKey});

            _logger.LogInformation($"Start receiving");
            await _longPollWatcher.StartWatchAsync();

            var messages = await _api.Messages.GetDialogsAsync(new MessagesDialogsGetParams
            {
                Count = 100,
                Unread = true
            });

            ReceiveMessages(this, messages.Messages);
        }

        private async Task Relogin()
        {
            if (_api == null || _longPollWatcher == null)
                return;

            _logger.LogInformation($"Relogin");
            await Login();
        }

        private void ReceiveMessages(object sender, IEnumerable<Message> e)
        {
            foreach (var message in e)
            {
                ReceiveMessage(message);
            }
        }

        public void ReceiveMessage(Message message)
        {
            var userId = (message.FromId ?? message.UserId ?? message.ChatId).GetValueOrDefault();
            var chatId = userId;

            if (message.Type == MessageType.Sended)
                return;

            var context = new Context
            {
                Text = message.Text ?? message.Body,
                Username = userId.ToString(),
                IsGroupChat = false,
                ChatId = chatId,
                ChatTitle = userId.ToString(),
                Provider = this
            };
            
            OnReceiveMessage(this, context);
        }

        public async Task SendMessageAsync(Context context, string message)
        {
            if (context.Buttons != null)
            {
                throw new NotSupportedException($"{nameof(VkBotProvider)} is not supported inline button");
            }

            await _api.Messages.SendAsync(new MessagesSendParams
            {
                UserId = context.ChatId,
                Message = message
            });
        }

        public Task EditLastMessageAsync(Context context, string message)
        {
            throw new NotImplementedException();
        }

        public Task SendFileAsync(Context context, string filename, Stream data)
        {
            throw new NotImplementedException();
        }
    }
}