using System;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Framework.Test.Utilites;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using SimpleBot.Framework;
using SimpleBot.Framework.Interfaces;
using SimpleBot.Framework.Models;
using Xunit;
using Xunit.Abstractions;

namespace Framework.Test
{
    public class Route
    {
        private readonly ITestOutputHelper _output;
        private readonly UnitTestProvider _provider;
        private readonly BotContextState _userState;
        private readonly BotCore _bot;

        public Route(ITestOutputHelper output)
        {
            _output = output;
            _userState = new BotContextState {StateName = ""};
            _provider = new UnitTestProvider(_output);

            var logger = new Mock<ILogger<BotCore>>();

            var opt = new Mock<IOptions<BotCoreConfig>>();
            opt.Setup(x => x.Value).Returns(new BotCoreConfig
            {
                BotGroupCommandPrefix = new[] {"bot"}
            });

            var contextStateProvider = new Mock<IContextStateProvider>();
            contextStateProvider.Setup(x => x.Get(It.IsAny<long>())).Returns(Task.FromResult(_userState));

            var serviceProvider = new Mock<IServiceProvider>();
            serviceProvider
                .Setup(x => x.GetService(It.IsAny<Type>()))
                .Returns(new MemoryController());

            _bot = new BotCore(opt.Object, logger.Object, serviceProvider.Object, contextStateProvider.Object);
            _bot.AddProvider(_provider);
        }

        [Theory]
        [InlineData("/base", "Base")]
        [InlineData("qwert", "Default")]
        [InlineData("priority", "Priority150")]
        [InlineData("/123-qwe", "Regex")]
        [InlineData("mult 4 3", "12")]
        [InlineData("paramDefaultValue 10", "10")]
        [InlineData("fromRouteAttribute_name 5 2", "3")]
        [InlineData("fromRouteAttribute_index 5 2", "3")]
        [InlineData("stringResponse", "Response")]
        public async Task TextMessage(string request, string response)
        {
            _bot.AddController<MemoryController>();
            await _bot.Run(false);

            CheckReceive(_provider, request, response);
        }

        [Fact]
        public async Task UserState()
        {
            _bot.AddControllersFromAssembly();
            await _bot.Run(false);
   
            _userState.StateName = "testState";
            CheckReceive(_provider, "/base", "BaseInState");

            _userState.StateName = "";
            CheckReceive(_provider, "/base", "Base");
        }
        
        [Fact]
        public async Task GroupChat()
        {
            _bot.AddController(typeof(MemoryController));
            await _bot.Run(false);
            
            CheckReceive(_provider, new Context {IsGroupChat = false, Text = "group"}, "group");
            CheckReceive(_provider, new Context {IsGroupChat = true, Text = "group"}, null);
            CheckReceive(_provider, new Context {IsGroupChat = true, Text = "bot, group"}, "group");
        }

        private void CheckReceive(UnitTestProvider unitTestProvider, string request, string controllerResult)
        {
            CheckReceive(unitTestProvider, new Context {Text = request}, controllerResult);
        }

        private void CheckReceive(UnitTestProvider unitTestProvider, Context request, string controllerResult)
        {
            lock (_output)
            {
                UnitTestProvider.LastExecuteAction = null;

                request.Provider = unitTestProvider;
                unitTestProvider.Receive(request);
                Assert.Equal(controllerResult, UnitTestProvider.LastExecuteAction);
            }
        }
    }
}