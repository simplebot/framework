using System.Threading.Tasks;
using SimpleBot.Framework;
using SimpleBot.Framework.Attributes;

namespace Framework.Test.Utilites
{
    public class MemoryController : BotControllerBase
    {
        [Phrase("/base")]
        public Task Base() => ReplyAsync("Base");

        [DefaultPhrase]
        public Task Default() => ReplyAsync("Default");

        [Phrase(1, "priority")]
        public Task Priority1() => ReplyAsync("Priority1");

        [Phrase(150, "priority")]
        public Task Priority150() => ReplyAsync("Priority150");
        
        [Phrase(@"/\d+-\w+")]
        public Task Regex() => ReplyAsync("Regex");

        [Phrase("group")]
        public Task Group() => ReplyAsync("group");

        [Phrase(@"mult (?<a>\d+) (?<b>\d+)")]
        public Task Mult(int a, int b) => ReplyAsync((a * b).ToString());

        [Phrase(@"paramDefaultValue (?<a>\d+)( (?<b>\d+))?")]
        public Task ParamDefaultValue(int a, int b) => ReplyAsync((a + b).ToString());

        [Phrase(@"fromRouteAttribute_name (?<pattenrn_name>\d+) (?<b>\d+)")]
        public Task FromRouteAttributeName(int b, [FromRoute("pattenrn_name")] int codeName) => ReplyAsync((codeName - b).ToString());

        [Phrase(@"fromRouteAttribute_index (\d+) (?<b>\d+)")]
        public Task FromRouteAttributeIndex(int b, [FromRoute(1)] int codeName) => ReplyAsync((codeName - b).ToString());

        [Phrase("/base")]
        [ForState("testState")]
        public Task StateBase() => ReplyAsync("BaseInState");

        [Phrase("stringResponse")]
        public Task<string> StringResponse() => Task.FromResult("Response");
    }
}