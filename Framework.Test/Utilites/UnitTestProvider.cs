using System;
using System.IO;
using System.Threading.Tasks;
using SimpleBot.Framework.Interfaces;
using SimpleBot.Framework.Models;
using Xunit.Abstractions;

namespace Framework.Test.Utilites
{
    class UnitTestProvider : IBotProvider
    {
        public static string LastExecuteAction;

        public event EventHandler OnReady = (s, c) => { };
        public event EventHandler<Context> OnReceiveMessage = (s, c) => { };
        private readonly ITestOutputHelper _output;

        public UnitTestProvider(ITestOutputHelper output)
        {
            _output = output;
        }

        public Task RunAsync()
        {
            OnReady?.Invoke(this, null);
            return Task.CompletedTask;
        }

        public Task SendMessageAsync(Context context, string message)
        {
            _output.WriteLine($"> {context.ChatId}: {message}");
            LastExecuteAction += message;
            return Task.CompletedTask;
        }

        public Task EditLastMessageAsync(Context context, string message)
        {
            _output.WriteLine($"> {context.ChatId}(Edit): {message}");
            LastExecuteAction += message;
            return Task.CompletedTask;
        }

        public Task SendFileAsync(Context context, string filename, Stream data)
        {
            _output.WriteLine($"> {context.ChatId}: {filename} {data.Length} bytes");
            LastExecuteAction += filename;
            return Task.CompletedTask;
        }

        public void Receive(string message)
        {
            Receive(new Context
            {
                Provider = this,
                Text = message
            });
        }

        public void Receive(long userId, string message)
        {
            Receive(new Context
            {
                Provider = this,
                Username = userId.ToString(),
                ChatId = userId,
                Text = message
            });
        }

        public void Receive(Context context)
        {
            OnReceiveMessage.Invoke(this, context);
        }
    }
}