﻿using System;

namespace SimpleBot.Framework.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class DefaultPhraseAttribute : PhraseAttribute
    {
        public DefaultPhraseAttribute() : base(int.MinValue, ".*")
        {
            
        }
    }
}