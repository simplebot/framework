﻿using System;

namespace SimpleBot.Framework.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class FilePhraseAttribute : PhraseAttribute
    {
        public FilePhraseAttribute(params string[] phraseMasks) : base(phraseMasks)
        {
        }

        public FilePhraseAttribute(int priority, params string[] phraseMasks) : base(priority, phraseMasks)
        {
        }
    }
}