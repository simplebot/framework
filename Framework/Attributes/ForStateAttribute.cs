﻿using System;

namespace SimpleBot.Framework.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ForStateAttribute : Attribute
    {
        public string[] StateNames { get; }
        
        public ForStateAttribute(params string[] stateNames)
        {
            StateNames = stateNames;
        }
    }
}