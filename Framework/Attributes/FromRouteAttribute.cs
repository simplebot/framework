﻿using System;

namespace SimpleBot.Framework.Attributes
{
    [AttributeUsage(AttributeTargets.Parameter)]
    public class FromRouteAttribute : Attribute
    {
        public string GroupName { get; set; }
        public int GroupIndex { get; set; }

        public FromRouteAttribute(string groupName)
        {
            GroupIndex = int.MinValue;
            GroupName = groupName;
        }

        public FromRouteAttribute(int groupIndex)
        {
            GroupIndex = groupIndex;
            GroupName = string.Empty;
        }
    }
}