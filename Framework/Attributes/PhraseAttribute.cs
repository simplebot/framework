﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace SimpleBot.Framework.Attributes
{
    /// <summary>
    /// Позволяет направить пользователя в отмеченный метод при прохождении регулярного выражения
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class PhraseAttribute : Attribute
    {
        public Regex[] PhraseMask { get; }
        public int Priority { get; }

        /// <summary>
        /// Позволяет направить пользователя в отмеченный метод при прохождении регулярного выражения
        /// </summary>
        /// <remarks>Для поподания в метод достаточно прохождения любого из выражений</remarks>
        /// <param name="phraseMasks">Список регулярных варажений</param>
        public PhraseAttribute(params string[] phraseMasks) : this(100, phraseMasks)
        {
        }

        /// <summary>
        /// Позволяет направить пользователя в отмеченный метод при прохождении регулярного выражения
        /// </summary>
        /// <remarks>Для поподания в метод достаточно прохождения любого из выражений</remarks>
        /// <param name="priority">Приоритет, чем выше тем более предпочтительным является метод</param>
        /// <param name="phraseMasks">Список регулярных варажений</param>
        public PhraseAttribute(int priority, params string[] phraseMasks)
        {
            PhraseMask = phraseMasks.Select(x => new Regex(x, RegexOptions.IgnoreCase)).ToArray();
            Priority = priority;
        }

        public Match Match(string input)
        {
            foreach (var regex in PhraseMask)
            {
                var match = regex.Match(input);

                if (match.Success)
                    return match;
            }

            return System.Text.RegularExpressions.Match.Empty;
        }
    }
}