﻿using System;

namespace SimpleBot.Framework.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ReplaceLastPhraseAttribute : Attribute
    {
    }
}