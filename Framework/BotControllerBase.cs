﻿using System.IO;
using System.Text;
using System.Threading.Tasks;
using SimpleBot.Framework.Models;

namespace SimpleBot.Framework
{
    public abstract class BotControllerBase
    {
        public Context Context { get; set; }

        protected async Task ReplyAsync(string message)
        {
            await Context.Provider.SendMessageAsync(Context, message);
        }

        protected Task SendFileAsync(string filename, Stream data)
        {
            return Context.Provider.SendFileAsync(Context, filename, data);
        }

        protected Task SendFileAsync(string filename, byte[] data)
        {
            var ms = new MemoryStream(data);
            return SendFileAsync(filename, ms);
        }

        protected Task SendFileAsync(string filename, string data)
        {
            return SendFileAsync(filename, Encoding.Default.GetBytes(data));
        }
    }
}