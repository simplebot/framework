﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SimpleBot.Framework.Attributes;
using SimpleBot.Framework.Exceptions;
using SimpleBot.Framework.Interfaces;
using SimpleBot.Framework.Models;

namespace SimpleBot.Framework
{
    public class BotCore : IBotCore
    {
        private readonly IContextStateProvider _contextStateProvider;
        private readonly IServiceProvider _serviceProvider;
        private readonly List<IBotProvider> _providers;
        private readonly Semaphore _closeSemaphore;
        private readonly ILogger<BotCore> _logger;
        private readonly BotCoreConfig _config;
        private readonly List<Type> _controllerTypes;
        private List<RouteElement> _routeMap;
        
        /// <summary>
        /// Событие, вызываемое после запуска всех провайдеров
        /// </summary>
        public event EventHandler OnReady = (s, e) => { };

        public BotCore(
            IOptions<BotCoreConfig> config,
            ILogger<BotCore> logger,
            IServiceProvider serviceProvider,
            IContextStateProvider contextStateProvider)
        {
            _providers = new List<IBotProvider>();
            _closeSemaphore = new Semaphore(0, 1);
            _serviceProvider = serviceProvider;
            _contextStateProvider = contextStateProvider;
            _controllerTypes = new List<Type>();
            _config = config.Value;
            _logger = logger;
        }

        public IEnumerable<Type> AddControllersFromAssembly()
        {
            var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes());
            types = types.Where(x => typeof(BotControllerBase).IsAssignableFrom(x));
            types = types.Where(x => !x.IsAbstract);
            var list = types.ToList();

            _controllerTypes.AddRange(list);

            return list;
        }

        public void AddController<T>() where T : BotControllerBase
        {
            _controllerTypes.Add(typeof(T));
        }

        public void AddController(Type type)
        {
            _controllerTypes.Add(type);
        }

        private List<RouteElement> BuildRouteMap(IEnumerable<Type> types)
        {
            var routeAnyPriorityMap = new List<Tuple<int, RouteElement>>();

            foreach (var method in types.Distinct().SelectMany(x => x.GetMethods()))
            {
                var phraseAttribute = method.GetCustomAttribute<PhraseAttribute>();
                if (phraseAttribute == null)
                    continue;

                var replaceLastAttribute = method.GetCustomAttribute<ReplaceLastPhraseAttribute>();
                var forStateAttribute = method.GetCustomAttribute<ForStateAttribute>();

                routeAnyPriorityMap.Add(Tuple.Create(phraseAttribute.Priority, new RouteElement
                {
                    ControllerType = method.DeclaringType,
                    Method = method,
                    IsFile = phraseAttribute is FilePhraseAttribute,
                    PhraseAttribute = phraseAttribute,
                    IsReplaceLast = replaceLastAttribute != null,
                    StateNames = forStateAttribute?.StateNames ?? new string[0]
                }));
            }

            return routeAnyPriorityMap.OrderByDescending(x => x.Item1).Select(x => x.Item2).ToList();
        }

        public async Task Run(bool lockAfterRun = true)
        {
            _routeMap = BuildRouteMap(_controllerTypes);

            foreach (var provider in _providers)
            {
                provider.OnReceiveMessage += OnReceiveMessage;
                await provider.RunAsync();
            }

            OnReady?.Invoke(this, null);

            if (lockAfterRun)
                _closeSemaphore.WaitOne(); // todo:: await
        }

        public void AddProvider(IBotProvider provider)
        {
            _providers.Add(provider);
        }

        private void OnReceiveMessage(object sender, Context context)
        {
            var task = OnReceiveMessageAsync(sender, context);
            task.ConfigureAwait(false);
            task.Wait();
        }

        private async Task OnReceiveMessageAsync(object sender, Context context)
        {
            if (string.IsNullOrEmpty(context.Text))
                return;

            _logger.LogDebug($"Recive message from {context.Username}: {context.Text}");

            if (context.IsGroupChat)
            {
                var msg = context.Text.ToLower();
                var prefix = _config.BotGroupCommandPrefix.FirstOrDefault(x => msg.StartsWith(x.ToLower()));

                if (prefix == null)
                    return;

                context.Text = context.Text.Substring(prefix.Length);
            }

            context.StateProvider = _contextStateProvider;
            context.BotCore = this;

            await FindController(context);
        }

        private async Task FindController(Context context)
        {
            var userState = (await context.GetState()).StateName;

            foreach (var element in _routeMap)
            {
                if (string.IsNullOrEmpty(context.FileName) == element.IsFile)
                    continue;

                if (string.IsNullOrEmpty(userState) && element.StateNames.Length > 0)
                    continue;

                if (!string.IsNullOrEmpty(userState) &&
                    element.StateNames.All(x => !x.Equals(userState, StringComparison.OrdinalIgnoreCase)))
                {
                    continue;
                }

                var match = element.PhraseAttribute.Match(context.Text.ToLower());

                if (!match.Success)
                    continue;

                context.Arguments = context.Text.Remove(match.Index, match.Length).Trim();
                context.RouteMatch = match;

                await RouteToMethod(context, element);
                return;
            }
        }

        private async Task RouteToMethod(Context context, RouteElement routeElement)
        {
            try
            {
                var controllerType = routeElement.ControllerType;
                var method = routeElement.Method;

                if (!(_serviceProvider.GetService(controllerType) is BotControllerBase ctrl))
                {
                    throw new Exception(
                        $"Contoller {controllerType.FullName} is not {nameof(BotControllerBase)}. WTF?!");
                }

                _logger.LogInformation($"Routering {context.Username} to {controllerType.Name}.{method.Name}");
                context.ReplaceLastMessage = routeElement.IsReplaceLast;
                ctrl.Context = context;

                var args = BuildMethodArgs(method, context);
                var result = method.Invoke(ctrl, args);
                await ReplyAsync(context, result);
            }
            catch (Exception e)
            {
                while (e != null)
                {
                    _logger.LogError(e.Message);
                    _logger.LogError(e.StackTrace);
                    e = e.InnerException;
                }
            }
        }

        private static async Task ReplyAsync(Context context, object result)
        {
            if (result == null)
                return;

            if (result is Task task)
            {
                if (task.Exception != null)
                    throw task.Exception;
                
                await task;

                if (task is Task<string> taskStringResult)
                {
                    var resultString = taskStringResult.Result;

                    if (!string.IsNullOrEmpty(resultString))
                    {
                        if (context.ReplaceLastMessage)
                        {
                            await context.Provider.EditLastMessageAsync(context, resultString);
                        }
                        else
                        {
                            await context.Provider.SendMessageAsync(context, resultString);
                        }
                    }
                }
            }
        }

        private object[] BuildMethodArgs(MethodInfo method, Context context)
        {
            var result = new List<object>();

            foreach (var param in method.GetParameters())
            {
                var routeAttr = param.GetCustomAttribute<FromRouteAttribute>();
                Group group = null;

                if (routeAttr != null)
                {
                    if (!string.IsNullOrEmpty(routeAttr.GroupName))
                    {
                        group = context.RouteMatch.Groups[routeAttr.GroupName];
                    }
                    else
                    {
                        group = context.RouteMatch.Groups[routeAttr.GroupIndex];
                    }
                }

                if (group == null)
                {
                    group = context.RouteMatch.Groups[param.Name.ToLower()];
                }

                if (group != null && group.Value != "")
                {
                    var value = ChangeType(group.Value, param.ParameterType);
                    result.Add(value);
                }
                else
                {
                    result.Add(GetDefault(param.ParameterType));
                }
            }

            return result.ToArray();
        }

        private static object ChangeType(object value, Type type)
        {
            if (value == null)
                return null;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                return Convert.ChangeType(value, Nullable.GetUnderlyingType(type));
            }

            return Convert.ChangeType(value, type);
        }

        private static object GetDefault(Type type)
        {
            return type.IsValueType ? Activator.CreateInstance(type) : null;
        }
    }
}