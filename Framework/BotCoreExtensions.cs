using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using SimpleBot.Framework.Interfaces;

namespace SimpleBot.Framework
{
    public static class BotCoreExtensions
    {
        public static IServiceCollection AddBot(this IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            services.TryAddSingleton<IContextStateProvider, ContextStateProvider>();
            services.TryAddSingleton<BotCore>();
            AddControllers(services);

            return services;
        }

        private static void AddControllers(IServiceCollection serviceCollection)
        {
            var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes());
            types = types.Where(x => typeof(BotControllerBase).IsAssignableFrom(x));
            types = types.Where(x => !x.IsAbstract);

            foreach (var type in types)
            {
                serviceCollection.AddTransient(type);
            }
        }
    }
}