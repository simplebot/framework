using System.Collections.Concurrent;
using System.Threading.Tasks;
using SimpleBot.Framework.Interfaces;
using SimpleBot.Framework.Models;

namespace SimpleBot.Framework
{
    public class ContextStateProvider : IContextStateProvider
    {
        private readonly ConcurrentDictionary<long, BotContextState> _storage;

        public ContextStateProvider()
        {
            _storage = new ConcurrentDictionary<long, BotContextState>();
        }

        public Task Set(long user, BotContextState state)
        {
            _storage.AddOrUpdate(user, state, (k, v) => state);
            
            return Task.CompletedTask;
        }

        public async Task<BotContextState> Get(long user)
        {
            if (!_storage.TryGetValue(user, out var state))
            {
                state = new BotContextState();
                await Set(user, state);
            }

            return state;
        }
    }
}