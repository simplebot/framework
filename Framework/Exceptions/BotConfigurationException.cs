using System;

namespace SimpleBot.Framework.Exceptions
{
    public class BotConfigurationException : Exception
    {
        public BotConfigurationException(string message = null) : base(message)
        {

        }
    }
}