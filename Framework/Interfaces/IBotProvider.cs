using System;
using System.IO;
using System.Threading.Tasks;
using SimpleBot.Framework.Models;

namespace SimpleBot.Framework.Interfaces
{
    public interface IBotProvider
    {
        event EventHandler OnReady;
        event EventHandler<Context> OnReceiveMessage;
        
        Task RunAsync();
        Task SendMessageAsync(Context context, string message);
        Task EditLastMessageAsync(Context context, string message);
        Task SendFileAsync(Context context, string filename, Stream data);
    }
}