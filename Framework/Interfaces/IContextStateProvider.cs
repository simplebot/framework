using System.Threading.Tasks;
using SimpleBot.Framework.Models;

namespace SimpleBot.Framework.Interfaces
{
    public interface IContextStateProvider
    {
        Task Set(long user, BotContextState state);
        Task<BotContextState> Get(long user);
    }
}