using System.Collections.Generic;

namespace SimpleBot.Framework.Models
{
    public class BotContextState
    {
        public string StateName { get; set; }
        public Dictionary<string, object> Data { get; set; }

        public BotContextState()
        {
            Data = new Dictionary<string, object>();
        }

        public T Get<T>(string key)
        {
            if (Data.TryGetValue(key, out var value))
            {
                return (T) value;
            }

            return default;
        }

        public void Set<T>(string key, T value)
        {
            if (Data.ContainsKey(key))
            {
                Data[key] = value;
            }
            else
            {
                Data.Add(key, value);
            }
        }
    }
}