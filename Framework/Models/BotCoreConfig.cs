﻿namespace SimpleBot.Framework.Models
{
    public class BotCoreConfig
    {
        public string[] BotGroupCommandPrefix { get; set; }
    }
}