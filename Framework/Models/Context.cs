﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SimpleBot.Framework.Interfaces;

namespace SimpleBot.Framework.Models
{
    public class Context
    {
        private BotContextState _state;

        public IBotCore BotCore { get; set; }
        public IBotProvider Provider { get; set; }

        public string Text { get; set; }
        public string Arguments { get; set; }
        public string Username { get; set; }
        public string ChatTitle { get; set; }
        public bool IsGroupChat { get; set; }
        public InlineQueryMode InlineQuery { get; set; }
        public bool ReplaceLastMessage { get; set; }
        public long ChatId { get; set; }
        public Match RouteMatch { get; set; }
        public List<List<InlineButton>> Buttons { get; set; }
        public IContextStateProvider StateProvider { get; set; }
        public Func<Task<byte[]>> FileGetter { get; set; }
        public string FileName { get; set; }

        public BotContextState State
        {
            get => GetState().Result;
            set => StateProvider.Set(ChatId, value).Wait();
        }

        public async Task<BotContextState> GetState()
        {
            return _state ?? (_state = await StateProvider.Get(ChatId));
        }
    }
}