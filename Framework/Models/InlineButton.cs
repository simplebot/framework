namespace SimpleBot.Framework.Models
{
    public class InlineButton
    {
        /// <summary>
        /// Button display text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// HTTP url to be opened when button is pressed
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Data to be sent in a callback query to the bot when button is pressed
        /// </summary>
        public string Data { get; set; }

        public InlineButton(string text)
        {
            Text = text;
        }

        public InlineButton(string text, string data)
        {
            Text = text;
            Data = data;
        }
    }
}