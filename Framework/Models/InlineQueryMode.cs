namespace SimpleBot.Framework.Models
{
    public enum InlineQueryMode
    {
        None,
        Query,
        Result
    }
}