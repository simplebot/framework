﻿using System;
using System.Reflection;
using SimpleBot.Framework.Attributes;

namespace SimpleBot.Framework.Models
{
    public class RouteElement
    {
        public Type ControllerType { get; set; }
        public MethodInfo Method { get; set; }
        public PhraseAttribute PhraseAttribute { get; set; }
        public bool IsReplaceLast { get; set; }
        public string[] StateNames { get; set; }
        public bool IsFile { get; set; }

        public override string ToString()
        {
            var status = string.Join(", ", StateNames);
            if (status.Length > 0)
                status = $" [{status}] ->";
            
            return $"{PhraseAttribute.Priority}{status} {ControllerType.Name}.{Method.Name}";
        }
    }
}