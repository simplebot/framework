*** SimpleBot.Framework
Фреймворк с простой реализацией MVC для постороения чат-ботов. Позволяет создавать ботов для таких сервисов как telegram и ВКонтакте

![coverage](https://gitlab.com/simplebot/framework/badges/master/coverage.svg?job=coverage)

*** Пример использования

```
public class StartController : BotControllerWithDatabase
{
        [DefaultPhrase]
        public async Task Default()
        {
            await ReplyAsync("Забыл команды? /help");
        }
        
        [Phrase("^/?help|^помощь|^стравка")]
        public async Task<string> GetHelp() => "Описание команд бота...."
}
```

