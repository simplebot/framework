ls -hl

if [ "$RUN" = "publish" ]
then
    dotnet nuget push /*.nupkg -k $NUGET_KEY -s https://api.nuget.org/v3/index.json
    
elif [ "$RUN" = "test" ]
then
    dotnet minicover instrument --workdir ../ --assemblies Framework.Test/bin/**/*.dll --sources */**/*.cs
    dotnet minicover reset
    dotnet test --no-build
    dotnet minicover report --workdir ../
else
    echo 'Undefine run configuration $RUN'
fi